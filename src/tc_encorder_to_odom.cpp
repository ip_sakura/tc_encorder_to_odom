//http://wiki.ros.org/ja/navigation/Tutorials/RobotSetup/Odom

#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Int32.h>

#define PI 3.1415

long int encorder_l = 0;
long int encorder_r = 0;
long int pre_encorder_l = 0;
long int pre_encorder_r = 0;
const double robot_d = 0.205;//m 
const double robot_r = 0.125;//m

void encorderRCallback(const std_msgs::Int32::ConstPtr& encorder)
{
  encorder_r = encorder->data;
}
void encorderLCallback(const std_msgs::Int32::ConstPtr& encorder)
{
  encorder_l = encorder->data;
}

int main(int argc, char** argv){
  ros::init(argc, argv, "odometry_publisher");

  ros::NodeHandle n;
  ros::Publisher odom_pub = n.advertise<nav_msgs::Odometry>("odom", 50);
  ros::Subscriber sub_l = n.subscribe("l_pulse", 1000, encorderLCallback);
  ros::Subscriber sub_r = n.subscribe("r_pulse", 1000, encorderRCallback);
  tf::TransformBroadcaster odom_broadcaster;

  double x = 0.0;
  double y = 0.0;
  double th = 0.0;

  double vx = 0;
  double vy = 0;
  double vth = 0;

  ros::Time current_time, last_time;
  current_time = ros::Time::now();
  last_time = ros::Time::now();

  ros::Rate r(1.0);
  while(n.ok()){

    ros::spinOnce();               // check for incoming messages
    current_time = ros::Time::now();

    //compute odometry in a typical way given the velocities of the robot
    double dt = (current_time - last_time).toSec();
    double delta_x = 0;
    double delta_y = 0;
    double delta_th = 0;



    
    /******xxxxx*****/
    double delta_palse_l = encorder_l - pre_encorder_l;
    double delta_palse_r = encorder_r - pre_encorder_r;
    double delta_time = dt;
    if (delta_palse_l == 0) {
      delta_palse_l = 1;
    }
    if (delta_palse_r == 0) {
      delta_palse_r = 1;
    }
    double wL = (double)delta_palse_l * 2*PI / 23000 / delta_time;
    double wR = (double)delta_palse_r * 2*PI / 23000 / delta_time;
    double vL = robot_r * wL;
    double vR = robot_r * wR;

    double vc = (vL + vR) / 2;
    double wc = (vL - vR) / 2 / robot_d;

    double delta_distance_r = vL * delta_time;
    double delta_distance_l = vR * delta_time;
    
    double delta_distance = (delta_distance_r + delta_distance_l) / 2;
    double delta_theta = (delta_distance_r - delta_distance_l) / 2 / robot_d;

    th += delta_theta;
    delta_x = delta_distance * cos(th + delta_theta/2);//x
    delta_y = delta_distance * sin(th + delta_theta/2);//y
    vx = vc * cos(th);
    vy = vc * sin(th);
    x += delta_x;
    y += delta_y;
    
    if(th > 2*PI)th = th - 2*PI;

    //since all odometry is 6DOF we'll need a quaternion created from yaw
    geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(th);

    //first, we'll publish the transform over tf
    geometry_msgs::TransformStamped odom_trans;
    odom_trans.header.stamp = current_time;
    odom_trans.header.frame_id = "odom";
    odom_trans.child_frame_id = "base_link";

    odom_trans.transform.translation.x = x;
    odom_trans.transform.translation.y = y;
    odom_trans.transform.translation.z = 0.0;
    odom_trans.transform.rotation = odom_quat;

    //send the transform
    odom_broadcaster.sendTransform(odom_trans);

    //next, we'll publish the odometry message over ROS
    nav_msgs::Odometry odom;
    odom.header.stamp = current_time;
    odom.header.frame_id = "odom";

    //set the position
    odom.pose.pose.position.x = x;
    odom.pose.pose.position.y = y;
    odom.pose.pose.position.z = 0.0;
    odom.pose.pose.orientation = odom_quat;

    //set the velocity
    odom.child_frame_id = "base_link";
    odom.twist.twist.linear.x = vx;
    odom.twist.twist.linear.y = vy;
    odom.twist.twist.angular.z = vth;

    //publish the message
    odom_pub.publish(odom);

    last_time = current_time;
    pre_encorder_l = encorder_l;
    pre_encorder_r = encorder_r;
   
    r.sleep();
  }
}
